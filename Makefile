suites = jessie stretch sid
archs = armhf
nodes := $(patsubst nodes/%.yml,%,$(wildcard nodes/*.yml))
profiles-di = $(foreach suite,$(suites),\
 $(patsubst %,content/%-$(suite)/preseed.cfg,$(subst -,/,$(nodes))))
profiles-script = $(patsubst %/preseed.cfg,%/script.sh,$(profiles-di))
profiles = $(profiles-di) $(profiles-script)

all: $(profiles)

# TODO: drop sid → stretch workaround when fixed in boxer
$(profiles-di): content/%/preseed.cfg: $(wildcard nodes/*.yml)
	mkdir -p $(dir $@)
	cd $(dir $@) \
		&& boxer compose \
			--nodedir $(CURDIR)/nodes \
			--suite $(patsubst sid,stretch,$(lastword $(subst -, ,$*))) \
			$(subst /,-,$(firstword $(subst -, ,$*)))
$(profiles-script): content/%/script.sh: content/%/preseed.cfg

clean::
	rm -rf content

.PHONY: all clean
